section .bss
buffer:     resb 0x1000
buffer_end: 

section .text
global _start

_start:     xor rax, rax            ; sys_read
            xor rdi, rdi            ; stdin
            mov rsi, buffer         ; pointer
            mov rdx, (buffer_end - buffer)
            syscall

            mov rcx, rax            ; rcx - index of last tested char
.trimloop:  dec rcx                 ; trim newlines from the end
            cmp byte [buffer+rcx], 0x0A
            jz .trimloop

            xor rax, rax            ; rax - number of matching characters
            xor rbx, rbx            ; bl - previous character
            mov bl, [buffer]
.mainloop:  mov dl, [buffer+rcx]    ; dl - current character
            cmp bl, dl
            jnz .nomatch
            add rax, rbx            ; when match
            sub rax, '0'            ; increment eax by ascii2int(bl)
.nomatch:   mov bl, dl              ; prev character = current character
            dec rcx
            jns .mainloop

.print      mov rsi, (buffer_end - 1) ; print result
            mov byte [rsi], 0xA     ; end everything with newline
.itoa:      mov rcx, 10             ; manual implementation of itoa
.divloop:   xor rdx, rdx
            idiv rcx
            add rdx, 0x30
            dec rsi
            mov byte [rsi], dl
            cmp rax, 0
            jnz .divloop

            mov rax, 1              ; sys_write
            mov rdi, 1              ; stdout
            ; rsi - already pointing to start of string
            mov rdx, buffer_end
            sub rdx, rsi            ; num of characters
            syscall

            mov rax, 0x3C           ; sys_exit
            mov rdi, 0              ; exit code
            syscall
